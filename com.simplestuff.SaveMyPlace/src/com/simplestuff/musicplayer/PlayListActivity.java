package com.simplestuff.musicplayer;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayListActivity extends ListActivity {
	// Songs list
	public ArrayList<HashMap<String, String>> songsList = new ArrayList<>();
	Cursor cursor = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playlist);
		String[] projection1 = {
				MediaStore.Audio.Playlists._ID,
				MediaStore.Audio.Playlists.NAME
		};

		CursorLoader cLoader = new CursorLoader(getApplicationContext(),
				MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, projection1,
				MediaStore.Audio.Playlists._ID, null, null);
		cursor = cLoader.loadInBackground();
		if (cursor != null ) {
			if (cursor.moveToFirst()) {
				do {
					HashMap<String, String> song = new HashMap<>();
					song.put("_ID", cursor.getString(0));
					song.put("NAME", cursor.getString(1));
					songsList.add(song);
					Log.w(this.getPackageName(), "id: " + cursor.getString(0) + " name: " + cursor.getString(1));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}

		// Adding menuItems to ListView
		ListAdapter adapter = new SimpleAdapter(this, songsList,
				R.layout.playlist_item, new String[] { "NAME" }, new int[] {
						R.id.songTitle });

		setListAdapter(adapter);

		// selecting single ListView item
		ListView lv = getListView();
		// listening to single listitem click
		lv.setOnItemClickListener(new OnItemClickListener() {

			//@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {				
				HashMap<String,String> item = songsList.get(position);
				
				// Starting new intent
				Intent in = new Intent(getApplicationContext(),
						SaveMyPlace.class);
				// Sending songIndex to PlayerActivity
				in.putExtra("playListId", item.get("_ID"));
				
				setResult(RESULT_OK, in);
				// Closing PlayListView
				finish();
			}
		});

	}
}
