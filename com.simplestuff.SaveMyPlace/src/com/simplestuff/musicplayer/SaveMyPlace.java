package com.simplestuff.musicplayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class SaveMyPlace extends Activity implements SeekBar.OnSeekBarChangeListener {

    public static final int PLAYER_CANNOT_START = 0;
    public static final int END_OF_PLAYLIST = 1;
    public static final int PLAYBACK_STATUS = 2;
    public static final int SERVICE_STOP = 3;
    public static final int PLAYBACK_LIST = 4;
    private static final int PLAYLIST_ACTIVITY = 100;
    private static final int TRACK_CHOOSER_ACTIVITY = 101;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    SQLiteDatabase myDB;
    Messenger mService = null;
    Cursor playListCursor;
    SavedPlaylist savedPlaylist;
    ArrayList<String> playingList = null;
    private ImageButton btnPlay;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle;
    private ImageView albumArt;
    private SeekBar songProgressBar;
    private TextView songTitleLabel;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;
    private TextView songArtistLabel;
    private TextView prevTitleLabel;
    private TextView prevArtistLabel;
    private TextView nextTitleLabel;
    private TextView nextArtistLabel;
    private TextView playListLabel;
    private ImageButton btnForward;
    private ImageButton btnBackward;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private ImageButton btnPlaylist;
    private Utilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds
    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private long curPlayListId;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, PlayerService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
                sendServiceMessage(PlayerService.START_PLAYING, 0, 0, null);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };

    private void moveBookmark(int direction, int position) {
        // don't update anything if we're at the start or end!
        if (direction == 0) {
            myLog("movebookmark update repeat to " + Boolean.toString(isRepeat));
            myLog("updateDbEntry call mb1 repeat set to " + Boolean.toString(isRepeat));
            SavedPlaylist.updateDbEntry(playListCursor, curPlayListId, myDB, position, isRepeat);
        } else if (SavedPlaylist.cursorTrackMotion(playListCursor, direction, isRepeat)) {
            myLog("updateDbEntry call mb2 repeat set to " + Boolean.toString(isRepeat));
            SavedPlaylist.updateDbEntry(playListCursor, curPlayListId, myDB, 0, isRepeat);
            setTrackInfo(playListCursor);
            songProgressBar.setProgress(0);
            songCurrentDurationLabel.setText(utils.milliSecondsToTimer(0));
        }
    }

    private void setTrackInfo(Cursor plC) {
        if (prevTitleLabel != null) {
            prevTitleLabel.setText("prevTitle");
            prevArtistLabel.setText("prevArtist");
            nextTitleLabel.setText("nextTitle");
            nextArtistLabel.setText("nextArtist");
        }
        Map<String, String> titles = SavedPlaylist.getTrackTitles(plC);

        prevTitleLabel.setText(titles.get(SavedPlaylist.PREV_TITLE_KEY));
        prevArtistLabel.setText(titles.get(SavedPlaylist.PREV_ARTIST_KEY));
        songTitleLabel.setText(titles.get(SavedPlaylist.CUR_TITLE_KEY));
        songArtistLabel.setText(titles.get(SavedPlaylist.CUR_ARTIST_KEY));
        nextTitleLabel.setText(titles.get(SavedPlaylist.NEXT_TITLE_KEY));
        nextArtistLabel.setText(titles.get(SavedPlaylist.NEXT_ARTIST_KEY));
        updateAlbumArtImage(null);
    }

    private void startPlaylistActivity() {
        Intent i = new Intent(getApplicationContext(), PlayListActivity.class);
        startActivityForResult(i, PLAYLIST_ACTIVITY);
    }

    private void startTrackChooserActivity() {
        Intent i = new Intent(getApplicationContext(), TrackChooserActivity.class);
        Bundle b = new Bundle();
        ArrayList<String> trackList = getTrackNameList();
        if (trackList == null) {
            return;
        }
        b.putStringArrayList("list", getTrackNameList());
        i.putExtra("bundle", (Parcelable) b);
        i.putExtra("position", Integer.toString(playListCursor.getPosition()));
        startActivityForResult(i, TRACK_CHOOSER_ACTIVITY);

    }

    void doBindService() {
        if (bindService(new Intent(this, PlayerService.class), mConnection, Context.BIND_AUTO_CREATE)) {
            btnPlay.setImageResource(R.drawable.ic_av_pause);
        }
    }

    void doUnbindService() {
        if (isServiceRunning()) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, PlayerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);

            mService = null;
        }
    }

    void stopPlaying() {
        Intent stopIntent = new Intent(SaveMyPlace.this, PlayerService.class);
        getApplicationContext().stopService(stopIntent);
        btnPlay.setImageResource(R.drawable.ic_av_play);
        loadLastPlaylist();
        playingList = null;
    }

    void myLog(String logStr) {
        Log.w(this.getPackageName(), logStr);
    }

    private void updateAlbumArtImage(Long albumId) {
        if (playListCursor == null) {
            return;
        }
        if (albumId == null) {
            albumId = playListCursor.getLong(7);
        }
        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId);
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), albumArtUri);
            if (bitmap != null) {
                bitmap = Bitmap.createBitmap(bitmap);
            }
            albumArt.setImageBitmap(bitmap);
        } catch (IOException fnfe) {
            bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.no_album_art);
        }

        albumArt.setImageBitmap(bitmap);
    }

    private void loadLastPlaylist() {
        SQLiteStatement stmt = myDB.compileStatement("select value from settings where name='LastPlaylist'");

        try {
            String queryResult = stmt.simpleQueryForString();
            if (queryResult.equals("none")) {
                startPlaylistActivity();
            } else {
                curPlayListId = Long.parseLong(queryResult);
                try {
                    savedPlaylist = new SavedPlaylist.Builder(getApplicationContext(), curPlayListId, myDB).build();
                } catch (EmptyPlaylistException epe) {
                    startPlaylistActivity();
                    return;
                }
                Log.w(getPackageName(), "loadLastPlaylist isRepeat=" + Boolean.toString(savedPlaylist.isRepeat()));
                if (savedPlaylist.isValid()) {
                    playListCursor = savedPlaylist.getPlaylistCursor();
                    if (savedPlaylist.isShuffle() != isShuffle) {
                        setShuffle(savedPlaylist.isShuffle());
                    }
                    if (savedPlaylist.isRepeat() != isRepeat) {
                        setRepeat(savedPlaylist.isRepeat());
                    }
                } else {
                    startPlaylistActivity();
                    return;
                }
                playListLabel.setText(savedPlaylist.getName());
                long duration = playListCursor.getLong(6);
                long position = savedPlaylist.getPosition();
                int progress = utils.getProgressPercentage(position, duration);
                songProgressBar.setProgress(progress);
                setTrackInfo(playListCursor);
                // need to set time labels
                songCurrentDurationLabel.setText(utils.milliSecondsToTimer(position));
                songTotalDurationLabel.setText(utils.milliSecondsToTimer(duration));
            }
        } catch (SQLiteDoneException e) {
            myDB.execSQL("INSERT INTO SETTINGS (name, value) values ('LastPlaylist','none')");
            startPlaylistActivity();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        // All player buttons
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        btnBackward = (ImageButton) findViewById(R.id.btnBackward);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        btnPlaylist = (ImageButton) findViewById(R.id.btnPlaylist);
        btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
        btnShuffle = (ImageButton) findViewById(R.id.btnShuffle);
        albumArt = (ImageView) findViewById(R.id.albumArt);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songTitleLabel = (TextView) findViewById(R.id.curTrackTitle);
        songArtistLabel = (TextView) findViewById(R.id.curTrackArtist);
        prevTitleLabel = (TextView) findViewById(R.id.prevTrackTitle);
        prevArtistLabel = (TextView) findViewById(R.id.prevTrackArtist);
        nextTitleLabel = (TextView) findViewById(R.id.nextTrackTitle);
        nextArtistLabel = (TextView) findViewById(R.id.nextTrackArtist);
        playListLabel = (TextView) findViewById(R.id.playlistName);

        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);

        songProgressBar.setMax(100);

        // Mediaplayer
        utils = new Utilities();

        // Listeners
        songProgressBar.setOnSeekBarChangeListener(this); // Important
        myDB = DatabaseHelper.getInstance(getApplicationContext()).getWritableDatabase();
    }

    void setupUI() {

        btnPlay.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                if (isServiceRunning()) {
                    stopPlaying();
                    doUnbindService();
                } else {
                    Intent startIntent = new Intent(SaveMyPlace.this, PlayerService.class);
                    startIntent.putExtra("playListId", curPlayListId);
                    startIntent.putExtra("isShuffle", isShuffle);
                    startPlayerService(startIntent);
                }
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {

            //		@Override
            public void onClick(View arg0) {
                sendServiceMessage(PlayerService.SEEK_MESSAGE, seekForwardTime, 0, null);
            }
        });

        btnBackward.setOnClickListener(new View.OnClickListener() {

            //			@Override
            public void onClick(View arg0) {
                sendServiceMessage(PlayerService.SEEK_MESSAGE, -1 * seekBackwardTime, 0, null);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {

            //			@Override
            public void onClick(View arg0) {
                trackMotion(1);
            }
        });

        btnNext.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                startTrackChooserActivity();
                return true;
            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {

            //	@Override
            public void onClick(View arg0) {
                trackMotion(-1);
            }
        });

        btnPrevious.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                // enable this when the activity is ready
                startTrackChooserActivity();
                return true;
            }
        });

        btnRepeat.setOnClickListener(new View.OnClickListener() {

            //			@Override
            public void onClick(View arg0) {
                setRepeat(!isRepeat);
                // repeat doesn't matter unless we're playing
                if (isServiceRunning()) {
                    sendServiceMessage(PlayerService.REPEAT_TOGGLE, ((isRepeat) ? 1 : 0), 0, null);
                } else {
                    moveBookmark(0, 0);
                }
            }
        });

        btnShuffle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                setShuffle(!isShuffle);
                if (isServiceRunning()) {
                    sendServiceMessage(PlayerService.SHUFFLE_TOGGLE, ((isShuffle) ? 1 : 0), 0, null);
                } else {
                    if (!isShuffle) {
                        myDB.execSQL("delete from shuffle_mapping where playlist_id=" + curPlayListId);
                    }
                    try {
                        playListCursor = new SavedPlaylist.Builder(getApplicationContext(), curPlayListId, myDB)
                                .shuffle(isShuffle)
                                .currentTrack(playListCursor.getInt(0))
                                .build().getPlaylistCursor();
                        setTrackInfo(playListCursor);
                    } catch (EmptyPlaylistException epe) {
                        // this should be ok.
                        setShuffle(false);
                    }
                }
            }
        });

        btnPlaylist.setOnClickListener(new View.OnClickListener() {

            //		@Override
            public void onClick(View arg0) {
                if (isServiceRunning()) {
                    Log.w(getPackageName(), "playlist button when playing");
                    stopPlaying();
                    doUnbindService();
                } else {
                    Log.w(getPackageName(), "playlist button when not playing");
                }
                startPlaylistActivity();
            }
        });
    }

    /**
     * Receiving song index from playlist view
     * and play the song
     */
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        // look up the songs in the playlist
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == PLAYLIST_ACTIVITY) {
            String id = data.getExtras().getString("playListId");
            curPlayListId = Long.valueOf(id);
            Intent startIntent = new Intent(this, PlayerService.class);
            startIntent.putExtra("playListId", curPlayListId);
            startPlayerService(startIntent);
        } else if (requestCode == TRACK_CHOOSER_ACTIVITY) {
            int nextPos = data.getExtras().getInt("trackMotion");
            moveToTrack(nextPos);
        }
    }

    /**
     *
     * */
    //@Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     */
    //@Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        //mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress handler
     */
    //@Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (isServiceRunning()) {
            sendServiceMessage(PlayerService.SET_POSITION, seekBar.getProgress(), 0, null);
        } else {
            double progress = ((double) seekBar.getProgress()) / seekBar.getMax();
            double dPos = playListCursor.getLong(6) * progress;
            int iPos = (int) dPos;
            moveBookmark(0, iPos);
        }
    }

    private void moveToTrack(int nextPos) {
        if (isServiceRunning()) {
            sendServiceMessage(PlayerService.MOVE_TO_TRACK, nextPos, 0, null);
            updateAlbumArtImage(null);
        } else {
            moveBookmark(nextPos - playListCursor.getPosition(), 0);
        }
    }

    private void trackMotion(int vector) {
        if (isServiceRunning()) {
            sendServiceMessage(PlayerService.TRACK_MESSAGE, vector, 0, null);
        } else {
            moveBookmark(vector, 0);
        }
    }

    public void onPause() {
        super.onPause();
        doUnbindService();
    }

    public void onResume() {
        super.onResume();
        loadLastPlaylist(); // make sure that every time the UI becomes visible we reload the current state
        if (isServiceRunning()) {
            doBindService();
        }
    }

    public void onStop() {
        super.onStop();
    }

    public void onStart() {
        super.onStart();
        setupUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void sendServiceMessage(int what, int arg1, int arg2, Object obj) {
        if (isServiceRunning()) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, what, arg1, arg2, obj);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // do something
                }
            }
        }
    }

    private void startPlayerService(Intent i) {
        if (mService == null) {
            ComponentName cName = startService(i);
            if (cName != null) {
                doBindService();
            }
        }
    }

    private boolean isServiceRunning(String serviceName) {
        boolean serviceRunning = false;
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : am.getRunningServices(Integer.MAX_VALUE)) {
            if (runningServiceInfo.service.getClassName().equals(serviceName)) {
                serviceRunning = true;
            }
        }
        return serviceRunning;
    }

    private boolean isServiceRunning() {
        return isServiceRunning("com.simplestuff.musicplayer.PlayerService");
    }

    private void tintButton(ImageButton button, boolean highlight) {
        int highlightColor = getResources().getColor(android.R.color.holo_blue_light);
        if (highlight) {
            button.setColorFilter(highlightColor);
        } else {
            button.setColorFilter(null);
        }
    }

    private void setRepeat(boolean newIsRepeat) {
        tintButton(btnRepeat, newIsRepeat);
        isRepeat = newIsRepeat;
    }

    private void setShuffle(boolean newIsShuffle) {
        tintButton(btnShuffle, newIsShuffle);
        isShuffle = newIsShuffle;
    }

    private ArrayList<String> getTrackNameList() {
        if (isServiceRunning()) {
            return playingList;
        } else {
            return SavedPlaylist.getTitleList(playListCursor);
        }
    }

    @SuppressLint("HandlerLeak")
    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case PLAYER_CANNOT_START:
                    AlertDialog.Builder dialog = new AlertDialog.Builder(SaveMyPlace.this);
                    dialog.setMessage("That playlist is empty, please select another.")
                            .setTitle("Error")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialoginterface, int i) {
                                    dialoginterface.dismiss();
                                    startPlaylistActivity();
                                }
                            }).show();
                    return;
                case END_OF_PLAYLIST:
                    // delete the db entry for this playlist
                    myDB.execSQL("delete from playlists where playlist_id = " + Long.toString(curPlayListId));
                    btnPlay.setImageResource(R.drawable.ic_av_play);
                    return;
                case PLAYBACK_LIST:
                    if (!(msg.obj instanceof Bundle)) {
                        return;
                    }
                    Bundle bundle = (Bundle) msg.obj;
                    playingList = bundle.getStringArrayList("list");
                    return;
                case PLAYBACK_STATUS:
                    if (!(msg.obj instanceof Bundle)) {
                        return;
                    }
                    Bundle bun = (Bundle) msg.obj;
                    ArrayList<String> msgMap = bun.getStringArrayList("list");
                    if (msgMap == null) {
                        return;
                    }
                    long duration = Long.valueOf(msgMap.get(1));
                    long position = Long.valueOf(msgMap.get(2));
                    String title = msgMap.get(0);
                    boolean newIsShuffle = Boolean.valueOf(msgMap.get(3));
                    boolean newIsRepeat = Boolean.valueOf(msgMap.get(4));
                    String playListName = msgMap.get(5);
                    String artist = msgMap.get(6);
                    prevArtistLabel.setText(msgMap.get(7));
                    prevTitleLabel.setText(msgMap.get(8));
                    nextArtistLabel.setText(msgMap.get(9));
                    nextTitleLabel.setText(msgMap.get(10));
                    long albumId = Long.parseLong(msgMap.get(11));
                    songTotalDurationLabel.setText(utils.milliSecondsToTimer(duration));
                    songCurrentDurationLabel.setText(utils.milliSecondsToTimer(position));
                    int progress = utils.getProgressPercentage(position, duration);
                    songProgressBar.setProgress(progress);
                    songTitleLabel.setText(title);
                    playListLabel.setText(playListName);
                    songArtistLabel.setText(artist);

                    if (newIsShuffle != isShuffle) {
                        setShuffle(newIsShuffle);
                    }
                    if (newIsRepeat != isRepeat) {
                        setRepeat(newIsRepeat);
                    }
                    updateAlbumArtImage(albumId);
                    return;
                case SERVICE_STOP:
                    // Changing button image to pause button
                    btnPlay.setImageResource(R.drawable.ic_av_play);
                    doUnbindService();
                    stopPlaying();
                    return;

                default:
                    super.handleMessage(msg);
            }
        }
    }
}